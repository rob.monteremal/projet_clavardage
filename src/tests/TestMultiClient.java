package tests;

import java.net.*;

import projet_clavardage.*;

public class TestMultiClient {

	public static void main(String[] args) throws Exception {
		int sPort = 3007;
		User remote;
		remote = new User(InetAddress.getByName("10.1.5.71"), sPort, "server");

		/*
		 * LocalUser localuser = new LocalUser(InetAddress.getByName("localhost"), 2000,
		 * "client"); new Client(localuser, remote); Thread.sleep(100); Channel
		 * currentChannel = Main.channels.get(0);
		 * System.out.println("Sending hello on channel "+currentChannel.outChannel);
		 * currentChannel.outChannel.sendMsg(new Message("Hello from "+"client",
		 * localuser)); //Sends a message
		 */

		// Creates 3 clients
		for (int i = 0; i < 1; i++) {
			TestMultiClient.createClient(i, sPort + i, "client" + i, remote);
			// Thread.sleep(100);
		}

	}

	// Creates a client and connects to remote server
	public static void createClient(int index, int port, String username, User remote) throws Exception {
		System.out.println(username + " created on port " + port);
		LocalUser localuser = new LocalUser(port, username);

		new Client(localuser, remote);
		Thread.sleep(500);

		Channel currentChannel = Main.getChannelList().get(index);
		System.out.println("Sending message on channel " + index);
		currentChannel.outChannel.sendMsg(new Message("To be fair, you have to have a very high IQ to understand Rick and Morty. The humour is extremely subtle, and without a solid grasp of theoretical physics most of the jokes will go over a typical viewer's head. There's also Rick's nihilistic outlook, which is deftly woven into his characterisation- his personal philosophy draws heavily from Narodnaya Volya literature, for instance. The fans understand this stuff; they have the intellectual capacity to truly appreciate the depths of these jokes, to realise that they're not just funny- they say something deep about LIFE. As a consequence people who dislike Rick & Morty truly ARE idiots- of course they wouldn't appreciate, for instance, the humour in Rick's existential catchphrase \"Wubba Lubba Dub Dub,\" which itself is a cryptic reference to Turgenev's Russian epic Fathers and Sons. I'm smirking right now just imagining one of those addlepated simpletons scratching their heads in confusion as Dan Harmon's genius wit unfolds itself on their television screens. What fools.. how I pity them. 😂\r\n" + 
				"\r\n" + 
				"And yes, by the way, i DO have a Rick & Morty tattoo. And no, you cannot see it. It's for the ladies' eyes only- and even then they have to demonstrate that they're within 5 IQ points of my own (preferably lower) beforehand. Nothin personnel kid from " + username, localuser)); // Sends a message
		Thread.sleep(2000);
		try	{
			Message msg = currentChannel.inChannel.popInputMessageBuffer();
			System.out.println("New message from "+msg.getSender().getUsername()+" at "+msg.getDate().toString()+" reading :"+msg.getContent());					
		} catch (Exception e)	{
		}
		currentChannel.outChannel.sendMsg(new Message("",localuser)); // Sends a message
		Thread.sleep(10000);
		currentChannel.outChannel.disconnect();
	}
}
