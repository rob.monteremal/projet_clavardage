package tests;

import java.net.*;

import projet_clavardage.*;

public class TestMultiServer {

	public static void main(String[] args) throws Exception {
		//Creates a list of 3 diff users
		Main.addUser(new User(InetAddress.getByName("10.1.5.74"), 2000, "client1"));
		Main.addUser(new User(InetAddress.getByName("10.1.5.74"), 2001, "client2"));
		Main.addUser(new User(InetAddress.getByName("10.1.5.74"), 2002, "client3"));

		LocalUser localuser;
		localuser = new LocalUser(3007, "server");
		
		//Creates a server on a new thread
		Server server = new Server(localuser);
		System.out.println("Starting server on port "+localuser.getPort());
		new Thread(server).start();

		//Loops around every input channel and prints the messages in the input buffers
		while(true)	{
			Thread.sleep(500);
			for (int i = 0 ; i<Main.getUserList().size() ; i++)	{
//				Thread.sleep(1000);
//				System.out.println(channel);
//				System.out.println(channel.inChannel);
				Channel channel = Main.getChannelList().get(i);
//				System.out.println(channel.inChannel.inputMessageBuffer.size());
				try	{
					Message msg = channel.inChannel.popInputMessageBuffer();
					User sender = msg.getSender();
					System.out.println("New message from "+sender.getUsername()+" at "+msg.getDate().toString()+" reading : "+msg.getContent());					
					System.out.print("User : "+sender.getUsername()+" at "+sender.getAddress().toString()+":"+sender.getPort());
					channel.outChannel.addOutputMessageBuffer(new Message("hello back", localuser));
					channel.outChannel.addOutputMessageBuffer(new Message("@Walleza", localuser));
					channel.outChannel.addOutputMessageBuffer(new Message("t gay", localuser));
				} catch (Exception e)	{
				}
			}
			/*
			for (Channel channel : Main.channels)	{
				Thread.sleep(10000);
				Message msg = channel.inChannel.popInputMessageBuffer();
				System.out.println("New message from "+msg.getSender().username+" at "+msg.getDate().toString()+" reading :"+msg.getContent());
			}
			*/
		}
	}
}
