package projet_clavardage;

import java.io.*;
import java.net.*;


/*
 * Creates a Client which is used to connect to a remote Server
 * The Client then creates a Channel and adds it to the list of channels
 */
public class Client {

	LocalUser localuser;
	Socket sock;
	User user;
	
	public Client(LocalUser localuser, User user) throws IOException {
		Socket sock = new Socket();
		this.localuser = localuser;
		this.user = user;

		sock.connect(new InetSocketAddress(user.getAddress(), user.getPort()));
		this.sock = sock;
	}

	
	// Creates a new Channel on another Thread and returns it
	public Channel createChannel()	{
		Channel channel = new Channel(this.localuser, this.user, this.sock);
		new Thread(channel).start();
		Main.addChannel(channel);

		return channel;
	}
}
