package projet_clavardage;

import java.io.*;
import java.net.*;

/*
 * Creates a Server which can accept connections from remote Clients
 * The Server then creates a Channel (in a new thread) and adds it to the list of channels
 * Goes back to accepting new connections after
 * ONLY A SINGLE INSTANCE SHOULD BE CREATED
 */
public class Server implements Runnable {

	private LocalUser localuser;
	private ServerSocket sock;
	private User user;
	private Socket client;
	private Boolean stop = false;
	
	protected Thread runningThread = null;
	
	public Server(LocalUser user)	{
		this.localuser = user;
	}
	
	public void run()	{
		synchronized(this)	{
			this.runningThread = Thread.currentThread();
		}
		
		// Creates a local server
		try	{			
			this.sock = new ServerSocket(localuser.getPort());
		} catch (IOException e)	{
			throw new RuntimeException("Cannot open a socket on port "+localuser.getPort());
		}

		// accept() loop
		while(!stop)	{
			client = null;
			
			try	{
				client = sock.accept();				
			//Connection with the client cannot be established
			} catch (IOException e)	{
				if(stop)	{
					System.out.println("Server stopped");
					return;
				} else {
					throw new RuntimeException("Cannot open a socket on port "+localuser.getPort());
				}
			}
			System.out.print("New connection received on "+localuser.getPort()+" at remote port "+client.getPort());

			//Creates a new Channel on another Thread
			user = Main.getUser(client);
			Channel channel = new Channel(localuser, user, client);
			new Thread(channel).start();
			Main.addChannel(channel);
		}
		System.out.println("Server stopped");		
	}
	
	public boolean SocketClosed() {
		if(this.client.isClosed()) { 
			System.out.println("This socket is closed");
			return true;
		} else {
			System.out.println("This socket is still running");
			return false;
		}
	}
	
}
