package projet_clavardage;

import java.io.*;
import java.net.*;
import java.util.Date;
import java.util.Observable;

import UI.GUI;

/*
 * Server running in the background used to control the discovery step
 * Runs on an hardcoded multicast address/port where user send message when they connect
 * Answers with its own username whenever a new user connects
 * Handles the unique username feature
 * ONLY A SINGLE INSTANCE SHOULD BE CREATED
 */
public class Discovery extends Observable implements Runnable {
	
	// Status of the current Discovery object
	private Discovery_Status status = Discovery_Status.disconnected;

	public void setStatus(Discovery_Status status)	{
		this.status = status;
	}
	
	public Discovery_Status getStatus()	{
		return this.status;
	}
	
	public void run() {
		ByteArrayOutputStream baos;
		ObjectOutputStream oos;
		ByteArrayInputStream bais;
		ObjectInputStream ois;
		byte[] data, buffer;		
		Message answer, multicastMessage;

		LocalUser localuser = Main.getLocalUser();
		boolean sendHello = true;
		status = Discovery_Status.connecting;
		
		// Address & port of multicast
		String multiCastAddress = "229.1.1.1";
		final int multiCastPort = 7777;
		final int bufferSize = 1024 * 4; // Maximum size of transfer object

		try {

			// Create Socket
			System.out.println("MulticastClient: Create socket on address " + multiCastAddress + " and port "
					+ multiCastPort + ".");
			InetAddress group = InetAddress.getByName(multiCastAddress);
			MulticastSocket s = new MulticastSocket(multiCastPort);
			DatagramSocket d = new DatagramSocket(localuser.getPort());
			s.joinGroup(group);
			
			
			// CLIENT discovery phase
			
			// Sets a timeout to wait for every response on the server
			d.setSoTimeout(Main.getTimeout());
		
			while (true) {

				if(sendHello)	{
					
					// Waits for the LoginWindow to resolve the username conflict
					while(Main.getDiscovery().getStatus() == Discovery_Status.username_conflict) {
						try {
							Thread.sleep(50);
						} catch (InterruptedException e1) {}
					}
					status = Discovery_Status.connecting;

					// Message to send on multicast channel
					multicastMessage = new Message("Hello from " + localuser.getUsername(), localuser);
	
					// Prepare Data
					baos = new ByteArrayOutputStream();
					oos = new ObjectOutputStream(baos);
					oos.writeObject(multicastMessage);
					data = baos.toByteArray();
	
					// Sends "Hello" message to every server on the multicast channel
					s.send(new DatagramPacket(data, data.length, group, multiCastPort));
					sendHello = false;
					
				} else {
					// Awaits an answer from the servers				
					try {
						System.out.println("MulticastClient: Waiting for datagram to be answered...");
	
						buffer = new byte[bufferSize];
						d.receive(new DatagramPacket(buffer, bufferSize, localuser.getAddress(), localuser.getPort()));
	
						// Deserialze received Message object
						bais = new ByteArrayInputStream(buffer);
						ois = new ObjectInputStream(bais);
						Object message = ois.readObject();
						
						// Checking if object is of Message type
						if (message instanceof Message) {
	
							//Extracting info from message
							User sender = ((Message) message).getSender();
							String username = sender.getUsername();
							Date date = ((Message) message).getDate();
							System.out.println("MulticastClient: Message is from: " + username + " at " + date.toString()
									+ " from : " + sender.getAddress().toString());

							// Checking if the chosen username is already in use
							if (((Message) message).getFlag() == 2)	{
								System.out.println("MulticastClient: Username "+localuser.getUsername()+" already in use");
								
								// We change the status to notify the GUI
								status = Discovery_Status.username_conflict;
								
								sendHello = true;
								continue;
							}
														
							// Adding sender to our userlist
							Main.addUser(new User(sender.getAddress(), sender.getPort(), sender.getUsername()));
							System.out.println(Main.getUserList().toString());
							
						// Someone is sending trash on the stream
						} else {
							System.out.println("MulticastClient: The received object is not of type Message!");
						}
						
					// If no answers have been received before set timeout, we end the client discovery phase
					} catch (SocketTimeoutException e) {
						status = Discovery_Status.connected;
						System.out.println("Timeout client, stopping discovery client side");

						break;
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				
				}
			}
			
			// SERVER discovery phase
			while (true) {
				status = Discovery_Status.listening;
				System.out.println("MulticastServer: Waiting for datagram to be received...");

				// Create buffer
				buffer = new byte[bufferSize];
				s.receive(new DatagramPacket(buffer, bufferSize, group, multiCastPort));

				// Deserialze object
				bais = new ByteArrayInputStream(buffer);
				ois = new ObjectInputStream(bais);
				
				//Waits for a "Hello" message from a user connecting
				try {
					Object message = ois.readObject();
					if (message instanceof Message) {
						User sender = ((Message) message).getSender();

						// Prevents loopback effect of multicast
						if (localuser.getAddress().equals(sender.getAddress())
								&& sender.getPort() == localuser.getPort()) {
							continue;
						}
						
						String username = sender.getUsername();
						
						// Unique username check
						if (Main.checkUsername(username) || localuser.getUsername().equals(username))	{
							System.out.println("MulticastServer: Username "+username+" already in use");
							
							// Sets Message flag to 2 to signify that the username is already in use
							answer = new Message("Username in use", localuser, 2);
							
						// Username isn't in use
						} else {
							Date date = ((Message) message).getDate();
							System.out.println("MulticastServer: Message is from: " + username + " at " + date.toString()
									+ " from : " + sender.getAddress().toString());
							
							// New user gets added to our local user list
							Main.addUser(new User(sender.getAddress(), sender.getPort(), sender.getUsername()));
							System.out.println(Main.getUserList().toString());
							GUI.UpdateUserList();
							// Gets notified of a new user to update the GUI user list
							notifyObservers();

							// ANSWER
							answer = new Message("Hello back", localuser);
						}
						
						// Opens a datagram socket/stream to send the answer
						d = new DatagramSocket();
						baos = new ByteArrayOutputStream();
						oos = new ObjectOutputStream(baos);
						oos.flush();

						// Sends the asnwer
						oos.writeObject(answer);
						data = baos.toByteArray();
						d.send(new DatagramPacket(data, data.length, sender.getAddress(), sender.getPort()));	

					} else {
						System.out.println("MulticastServer: The received object is not of type Message!");
					}
				} catch (IOException e) {
					System.out.println("MulticastServer: No object could be read from the received UDP datagram.");
					e.printStackTrace();
				}
			}
			
		} catch (Exception e) {
			System.out.println("Error: Multicast.");
			e.printStackTrace();
		}
	}
}