package projet_clavardage;

/*
 * Enum used to control the Discovery object for the log in step
 */
public enum Discovery_Status {
	disconnected,	// Discovery thread isn't started up
	intilializing,	// Thread is started but still initializing
	connecting,		// Thread is trying to connect
	username_conflict,	// Tried to connect but encountered a username issue
	username_changed,	// The username conflict has been solved
	connected,		// Tried to connect and was successful
	listening;		// Connected and waiting for other users
}
