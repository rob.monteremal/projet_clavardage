package projet_clavardage;

import java.net.*;

/*
 * Channel represents the connection to another user
 * It has a ChannelInput object and ChannelOuput object associated to it
 * Can be created by a Server object or a Client object
 */
public class Channel extends Thread {

	protected LocalUser localuser;
	protected User user;
	protected Socket socket;
	private boolean running = true;

	public ChannelOutput outChannel;
	public ChannelInput inChannel;

	public Channel(LocalUser localuser, User user, Socket socket) {
		this.localuser = localuser;
		this.user = user;
		this.socket = socket;
	}

	public void run() {
		// Input and output channels running on 2 seperate threads
		this.outChannel = new ChannelOutput(localuser, socket, this);
		new Thread(this.outChannel).start();

		this.inChannel = new ChannelInput(socket, this);
		new Thread(this.inChannel).start();

	}
	
	public User getUser() {
		return user;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
}
