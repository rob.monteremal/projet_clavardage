package projet_clavardage;

import java.net.InetAddress;
import java.net.DatagramSocket;

/*
 * Special form of a User representing the user of the current application
 * Has more setters that a User object doesn't have
 * ONLY A SINGLE INSTANCE SHOULD BE CREATED
 */
public class LocalUser extends User {

	// Needed for serialization
	private static final long serialVersionUID = 1L;

	// Constructor
	public LocalUser(int port, String username) {
		super(port, username);

		// Used to get the local address of the computer
		try (final DatagramSocket socket = new DatagramSocket()) {
			socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
			String ip = socket.getLocalAddress().getHostAddress();
			this.setAddress(InetAddress.getByName(ip));
		} catch (Exception e) {
		}
	}

	public void setUsername(String name) {
		this.username = name;
	}
}
