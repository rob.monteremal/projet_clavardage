package projet_clavardage;

import java.util.List;

import UI.GUI;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Collections;

/*
 * Container for most static methods that need to be accessed from anywhere in the program
 * Contains the list of Channel objects, User objects
 * Contais the LocalUser object
 */
public class Main {

	private static List<Channel> channels = Collections.synchronizedList(new ArrayList<Channel>());
	private static List<User> users = Collections.synchronizedList(new ArrayList<User>());
	private static LocalUser localuser = new LocalUser(10040, "Default");

	private static Discovery networkDiscovery = new Discovery();
	private static Server server;
	 
	private static int timeout = 1000;

	// Returns the list of users
	public static List<User> getUserList() {
		return users;
	}
	
	// Returns the list of channels
	public static List<Channel> getChannelList() {
		return channels;
	}
	
	// Returns the localuser
	public static LocalUser getLocalUser() {
		return localuser;
	}

	// Returns the network discovery object
	public static Discovery getDiscovery()	{
		return networkDiscovery;
	}
	
	// Return the accept server
	public static Server getServer()	{
		return server;
	}
	
	// Return the User of a specific socket
	public static User getUser(Socket client) {
		for(User user : users) {
			if(client.getInetAddress().equals(user.getAddress())) {
				return user;
			}
		}
		System.out.println("GetUser: User not found.");
		return null;
	}	
	
	// Opens a new accept server on another thread
	public static void openServer()	{
		server = new Server(localuser);
		new Thread(server).start();
	}
	
	// Sets the localuser
	public static void setLocaluser(LocalUser lu)	{
		localuser = lu;
	}
	
	// Adds a channel to the list of channels
	public static void addChannel(Channel channel) {
		channels.add(channel);
	}

	// Removes a channel from the list of channels
	public static void removeChannel(Channel channel) {
		channels.remove(channel);
	}

	// Adds a user to the list of users
	public static void addUser(User user) {
		users.add(user);
	}

	// Removes a user from the list of users
	public static void removeUser(User user) {
		users.remove(user);
	}
	
	// Checks if a username corresponds to a user in the user list
	public static boolean checkUsername(String username)	{
		for (User u : users)	{
			if (u.getUsername().equals(username))	{
				return true;
			}
		}
		return false;
	}
	
	// Tries to find the channel corresponding to the given user
	// If the channel doesn't exist, it gets opened via a Client object as usual
	public static Channel openChannel (User user)	{
		for (Channel ch : channels)	{
			if (ch.getUser().getUsername().equals(user.getUsername()))	{
				return ch;
			}
		}
		try {
			return new Client(localuser, user).createChannel();
		} catch (IOException e) {
			System.out.println("Server is NOT listening @IP:" + user.getAddress().toString() + " on port " + user.getPort());
			Main.removeUser(user);
			GUI.UpdateUserList();
			return null;
		}
	}

	// Tries to find the channel corresponding to the given user
	public static Channel findChannel (User user)	{
		for (Channel ch : channels)	{
			if (ch.getUser().getUsername().equals(user.getUsername()))	{
				return ch;
			}
		}
		return null;
	}

	public static int getTimeout() {
		return timeout;
	}

	public static void setTimeout(int timeout) {
		Main.timeout = timeout;
	}
	
}
