package projet_clavardage;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.EmptyStackException;
import java.util.Stack;

/*
 * ChannelOutput serves as the output stream of a Channel
 * It can send Message objects
 */
public class ChannelOutput implements Runnable {

	private LocalUser localuser;
	private Socket socket;
	private ObjectOutputStream out;
	private Channel channel;

	// TODO: Change the Stack to a LinkedList
	// Out buffer for the Message queue
	private Stack<Message> outputMessageBuffer = new Stack<Message>();

	public ChannelOutput(LocalUser localuser, Socket socket, Channel channel) {
		this.socket = socket;
		this.localuser = localuser;
		this.channel = channel;

		try {
			this.out = new ObjectOutputStream(socket.getOutputStream());
			this.out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Sends a Message object
	public void sendMsg(Message msg) {
		try {
			this.out.writeObject(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Sends a disconnect request
	public void disconnect() {
		this.sendMsg(new Message("", this.localuser, 1));
		try {
			this.channel.setRunning(false);
			System.out.println("Sending disconnect request");
			this.socket.close();
		} catch (IOException e) {
		}
	}

	public void run() {
		while (this.channel.isRunning()) {
			// Tries to send a message from the buffer
			try {
				this.sendMsg(this.popOutputMessageBuffer());
			} catch (EmptyStackException e) {
			} catch (Exception e)	{
				e.printStackTrace();
			}
		}
	}

	// Adds a message to the sending buffer
	public void addOutputMessageBuffer(Message msg) {
		outputMessageBuffer.push(msg);
		
		// When the message is outgoing we mark at with a direction of 1
		DatabaseConnectivity.addMessage(msg, 1, this.channel.getUser().getUsername());
	}

	// Pops a message from the sending buffer
	public Message popOutputMessageBuffer() {
		return outputMessageBuffer.pop();
	}
}
