package projet_clavardage;

import java.io.Serializable;
//import java.net.InetAddress;
import java.util.Date;

/*
 * Object that is sent between the users of the application
 * Contains a User object representing whoever sent the message
 * Needs to be Serializable to be sent as an object
 */
public class Message implements Serializable {

	// Needed for serialization
	private static final long serialVersionUID = 1L;

	private String content;
	private Date date;
	private User sender;
	
	/*
	 * Flag system used to convey special messages
	 * 0 - No flag
	 * 1 - Disconnect request
	 * 2 - Username already in use
	 */
	private int flag = 0;

	public Message(String content, User sender) {
		this.content = content;
		this.sender = sender;
		this.date = new Date();
	}

	public Message(String content, User sender, int flag) {
		this.content = content;
		this.sender = sender;
		this.date = new Date();
		this.flag = flag;
	}

	public Message(String content, User sender, Date date) {
		this.content = content;
		this.sender = sender;
		this.date = date;
	}

	public Message() {
	}

	public String getContent() {
		return this.content;
	}

	public Date getDate() {
		return this.date;
	}

	public User getSender() {
		return this.sender;
	}

	public int getFlag() {
		return this.flag;
	}
	
	public String toString()	{
		return "Message : "+this.getContent()+" from "+this.getSender().getUsername()+" at "+this.getDate().toString();
	}
}
