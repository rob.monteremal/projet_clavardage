package projet_clavardage;

import java.io.Serializable;
import java.net.InetAddress;

/*
 * Represents a remote user for the programm
 * Contains all the necessary information to communicate : IP, Port, username
 * Needs to be Serializable to be sent as an object
 */
public class User implements Serializable {

	// Needed for serialization
	private static final long serialVersionUID = 1L;

	private InetAddress address;// IP address
	private int port; // TCP port

	protected String username;
	public String status; // connected, etc

	// Constructor
	public User(InetAddress address, int port, String username) {
		this.address = address;
		this.port = port;
		this.username = username;
	}

	public User(int port, String username) {
		this.port = port;
		this.username = username;
	}

	public User(LocalUser local) {
		this.address = local.getAddress();
		this.port = local.getPort();
		this.username = local.getUsername();
	}
	
	public User(String username)	{
		this.username = username;
	}

	public InetAddress getAddress() {
		return this.address;
	}

	public void setAddress(InetAddress address) {
		this.address = address;
	}

	public int getPort() {
		return this.port;
	}

	public String getUsername() {
		return this.username;
	}
}
