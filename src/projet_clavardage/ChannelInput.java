package projet_clavardage;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.Stack;

import UI.GUI;

/*
 * ChannelInput serves as the input stream of a Channel
 * It can receive Message objects
 */
public class ChannelInput implements Runnable {

	private Socket socket;
	private ObjectInputStream in;
	private Channel channel;

	// TODO: Change the Stack to a LinkedList
	// Input buffer for the Message queue
	public Stack<Message> inputMessageBuffer = new Stack<Message>();

	public ChannelInput(Socket socket, Channel channel) {
		this.socket = socket;
		this.channel = channel;

		try {
			InputStream is = socket.getInputStream();
			try {
				this.in = new ObjectInputStream(is);
			} catch (IOException e) {
				System.out.println(
						"Error : Socket remote port " + socket.getPort() + " local port " + socket.getLocalPort());
				System.out.println("Error : ObjectInputStream creation");
				e.printStackTrace();
				System.exit(0);
			}
		} catch (IOException e) {
			System.out.println("Error : InputStream creation");
			e.printStackTrace();
		}
	}

	// Receives a Message object
	public Message receiveMsg() {
		Message msgInput = new Message();

		try {
			// Blocking function (no need for a sleep)
			msgInput = (Message) this.in.readObject();

			// If we received a disconnect request
			if (msgInput.getFlag() == 1) {
				try {
					this.channel.setRunning(false);
					Main.removeUser(Main.getUser(socket));
					this.socket.close();
					Main.removeChannel(this.channel);
					System.out.println("Closing socket");
					GUI.UpdateUserList();

				} catch (IOException e) {
				}
			}
		} catch (SocketException e) {
			// SocketException is expected to occur when we close the Channel
			if (this.channel.isRunning()) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return msgInput;
	}

	public void run() {
		while (this.channel.isRunning()) {
			try {
				Message message = this.receiveMsg();
				this.addInputMessageBuffer(message);
				GUI.RcvMsg(message, 0);
			} catch (Exception e) {
				if (this.channel.isRunning() == true) {
					System.out.println("Error : Adding Message to InputMessageBuffer");
					e.printStackTrace();
				}
			}
		}
	}

	// Adds a message to the receiving buffer
	public void addInputMessageBuffer(Message msg) {
		inputMessageBuffer.push(msg);
		
		// When the message is ingoing we mark at with a direction of 0
		DatabaseConnectivity.addMessage(msg, 0, msg.getSender().getUsername());
	}

	// Pops a message from the sending buffer
	public Message popInputMessageBuffer() {
		return inputMessageBuffer.pop();
	}
}
