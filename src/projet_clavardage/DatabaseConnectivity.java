package projet_clavardage;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import UI.GUI;

/*
 * Used to control the Database
 * ONLY A SINGLE INSTANCE SHOULD BE CREATED
 */
public class DatabaseConnectivity {

	static Connection conn = null;
	static DatabaseMetaData meta = null;
	static Statement stmt = null;
	static PreparedStatement pstmt = null;
	
	// Boolean representing wether the JDBC works on the machine or not
	// if it insn't installed (database_enabled = false), methods don't actually do anything
	static boolean database_enabled = true;

	// Creates the whole SQLite database
	public static void createDatabase()
	{
		String url = "jdbc:sqlite:messages.db";
		try{
			DatabaseConnectivity.conn = DriverManager.getConnection(url);
			if (conn != null)
			{
				DatabaseConnectivity.meta = conn.getMetaData();
				System.out.println("[DB] Database created");
				DatabaseConnectivity.stmt = conn.createStatement();
				
				// Creates the MESSAGE table if it doesn't exist
				String req = "CREATE TABLE IF NOT EXISTS MESSAGE(id integer PRIMARY KEY, date LONG, content text, direction integer, user text);";
				stmt.execute(req);
				
			}
		} catch (SQLException e)
		{
			database_enabled = false;
			System.out.println("[DB] Error creating the database");
			System.out.println(e.getMessage());
		}
	}

	// Adds a Message to the database
	public static void addMessage(Message m, int direction, String username)
	{
		if (database_enabled)	{		
			try {
				// Inserts the Message in the MESSAGE SQL table
				DatabaseConnectivity.pstmt = conn.prepareStatement("INSERT INTO MESSAGE (date, content, direction, user) VALUES (?,?,?,?)");
				DatabaseConnectivity.pstmt.setLong(1, m.getDate().getTime());
				DatabaseConnectivity.pstmt.setString(2, m.getContent());
				DatabaseConnectivity.pstmt.setInt(3, direction);
				DatabaseConnectivity.pstmt.setString(4, username);

				pstmt.executeUpdate();
			} catch (SQLException e) {
				System.out.println("[DB] Error inserting message into database");
				e.printStackTrace();
			}	
		}
	}
	
	// Retrieves the list of Message associated to a given User and feeds it into the GUI
	public static void getMessages(User u)	{
		if(database_enabled)	{
			// Crates a SQL request for every Message corresponding to the User
			String sql = "SELECT date, content, direction, user FROM MESSAGE WHERE user = '"+u.getUsername()+"'";
			System.out.println("SQL Request: "+sql);
			try {
				DatabaseConnectivity.stmt = DatabaseConnectivity.conn.createStatement();
				ResultSet rs = DatabaseConnectivity.stmt.executeQuery(sql);
				
				// Reads the messages from the DB and creates the associated Message object
				while (rs.next())	{
					User user = new User(rs.getString("user"));
					Message msg = new Message(rs.getString("content"), user, new Date(rs.getLong("date")));
					
					// The message is fed to the GUI RcvMsg method to handle it like an incoming message
					GUI.RcvMsg(msg, rs.getInt("direction"));
				}
				System.out.println("[DB] Successfuly retrieved the message history for "+u.getUsername()+"!");
				
			} catch (SQLException e) {
				System.out.println("[DB] Error retrieving the message list for "+u.getUsername());
				e.printStackTrace();
			}
		}		
	}
}
