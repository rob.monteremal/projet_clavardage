package UI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import projet_clavardage.*;
import javax.swing.UIManager;
import javax.swing.SwingConstants;

/*
 * Graphic interface for the log in window before opening the main GUI window
 * Designed in Java Swing using WindowBuilder module
 * ONLY A SINGLE INSTANCE SHOULD BE CREATED
 */
public class LoginWindow extends JFrame {

	// Used for serialization
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField Usernamefield;
	public static LoginWindow frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new LoginWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 430, 170);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Usernamefield = new JTextField();
		Usernamefield.setBounds(127, 12, 272, 26);
		contentPane.add(Usernamefield);
		Usernamefield.setColumns(10);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setBounds(12, 17, 117, 15);
		contentPane.add(lblUsername);

		JLabel PopUpLabel = new JLabel("");
		PopUpLabel.setHorizontalAlignment(SwingConstants.CENTER);
		PopUpLabel.setText("Please enter username...");
		PopUpLabel.setBounds(0, 58, 428, 15);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setBackground(UIManager.getColor("CheckBox.foreground"));
		contentPane.getRootPane().setDefaultButton(btnLogin);

		ActionListener btnActionListener = 
			new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Button is "locked" while we perform required operations
				btnLogin.setEnabled(false);
				Main.getLocalUser().setUsername(Usernamefield.getText());
				
				// The test is here to prevent SQL injection via the username
				if (Main.getLocalUser().getUsername().contains("'") || Main.getLocalUser().getUsername().contains("\"") || Main.getLocalUser().getUsername().contains(";"))	{
					PopUpLabel.setText("Forbidden character used, please enter another username");
				} else {
					// If the Discovery thread hasn't been started yet (first login attempt)
					if (Main.getDiscovery().getStatus() == Discovery_Status.disconnected)	{
						// Network discovery is started in a new thread
						new Thread(Main.getDiscovery()).start();
						System.out.println("[Login] Starting discovery server");
					}

					// If we had a previous conflict that we are now trying to solve
					else if (Main.getDiscovery().getStatus() == Discovery_Status.username_conflict)	{
						Main.getDiscovery().setStatus(Discovery_Status.username_changed);
					}

					// Tries to connect w/ current username
					PopUpLabel.setText("Connecting, please wait");
					System.out.println("[Login] Waiting on discovery server");

					// Waits on the other thread to notify that the status has been updated
					while(Main.getDiscovery().getStatus() == Discovery_Status.connecting || Main.getDiscovery().getStatus() == Discovery_Status.username_changed) {
						try {
							Thread.sleep(50);
						} catch (InterruptedException e1) {}
					}

					System.out.println("[Login] Status after loop: "+Main.getDiscovery().getStatus().toString());

					// If connected, we can then proceed to the main GUI
					if (Main.getDiscovery().getStatus() == Discovery_Status.connected || Main.getDiscovery().getStatus() == Discovery_Status.listening)	{
						GUI newWindow = new GUI();

						// Action to do when the newwindow is closed.
						newWindow.addWindowListener(new java.awt.event.WindowAdapter() {
						    @Override
						    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
						        if (JOptionPane.showConfirmDialog(newWindow, 
						            "Are you sure you want to close this window?", "Close Window?", 
						            JOptionPane.YES_NO_OPTION,
						            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
						        	for(Channel channel: Main.getChannelList()) {
						        		channel.outChannel.disconnect(); 
						        	}
						            System.exit(0);
						        }
						    }
						});
						newWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
						
						System.out.println("[Login] Starting server on port "+ Main.getLocalUser().getPort()+" as "+Main.getLocalUser().getUsername());
						Main.openServer();
						DatabaseConnectivity.createDatabase();
						
						// We hide current window
						newWindow.setVisible(true);
						frame.setVisible(false);
	
					// Problem with the connection : username already used on the network
					} else if (Main.getDiscovery().getStatus() == Discovery_Status.username_conflict) {
						System.out.println("[Login] Not connected (username taken)");
						PopUpLabel.setText("Username already taken! Please enter another username.");
					// Problem with the connection : Unused case (shouldn't be triggered)
					} else {
						System.out.println(Main.getDiscovery().getStatus().toString());
					}
				}
				// Button is then unlocked
				btnLogin.setEnabled(true);
			}
		};
		
		btnLogin.addActionListener(btnActionListener);
		btnLogin.setBounds(141, 85, 147, 44);
		contentPane.add(btnLogin);
		
		contentPane.add(PopUpLabel);
	}
}
