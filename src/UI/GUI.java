package UI;

import java.awt.EventQueue;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import projet_clavardage.*;
import java.awt.Color;
import java.awt.Font;

/*
 * Main graphic interface of the program
 * Designed in Java Swing using WindowBuilder module
 * ONLY A SINGLE INSTANCE SHOULD BE CREATED
 */
public class GUI extends JFrame {

	// Used for serialization
	private static final long serialVersionUID = 1L;
	private static JPanel contentPane;
	private static JTextField txtMessage;
	private static JLabel lblChat, lblUsers, lblUsername;
	private static JButton btnDM, btnSend;
	private static User remoteuser;
	private static Channel channel;
	private static JList<String> Userlist;
	private static JTextPane Chat;
	private static DefaultListModel<String> DLM;
	private JScrollPane scrollPane;
	private static boolean darkmode = false;
	private static SimpleAttributeSet sender, sender_metadata, sender_username, receiver, receiver_metadata, receiver_username;

	/**
	 * TEST ONLY:
	 * Launch the application without loging in
	 * The application should always be launch via LoginWindow to work properly
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		remoteuser = new User(55555, "Default");
		this.initFont();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 740, 537);
		
		// Main panel
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Label top of the user list
		lblUsers = new JLabel("Users:");
		lblUsers.setBounds(564, 26, 70, 15);
		contentPane.add(lblUsers);
		
		// Scrollable panel (used for the chat panel)
		scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 53, 529, 353);
		contentPane.add(scrollPane);
		
		// Chat panel
		Chat = new JTextPane();
		scrollPane.setViewportView(Chat);
		Chat.setEditable(false);
		
		// Text inside the chat panel
		txtMessage = new JTextField();
		txtMessage.setBounds(22, 419, 390, 75);
		contentPane.add(txtMessage);
		txtMessage.setColumns(10);
		txtMessage.setEditable(false);
		
		// Dark mode toggle button
		btnDM = new JButton("Dark Mode");
		btnDM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)	{
				GUI.toggleDarkMode();
			}
		});
		
		btnDM.setBounds(563, 450, 155, 43);
		contentPane.getRootPane().setDefaultButton(btnDM);
		contentPane.add(btnDM);
		
		// Send a message 
		btnSend = new JButton("Send");
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String content = txtMessage.getText();
				txtMessage.setText("");
				Message message = new Message(content, Main.getLocalUser());
				GUI.RcvMsg(message, 1);
				channel.outChannel.addOutputMessageBuffer(message);;
			}
		});
		btnSend.setEnabled(false);
		
		// User list panel
		Userlist = new JList<String>();
		Userlist.setBounds(563, 53, 155, 385);
		Userlist.setBorder(new MatteBorder(1, 1, 1, 1, (Color) UIManager.getColor("Button.darkShadow")));
		contentPane.add(Userlist);
		
		// -- We fill the userlist : -- //
		DLM = new DefaultListModel<String>();
		for(User user : Main.getUserList()) {
			DLM.addElement(user.getUsername());
		}
		Userlist.setModel(DLM);
		
		// -- We listen for double-clicks on a user of the userlist -- //
		Userlist.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				JList list = (JList) evt.getSource();
				if (evt.getClickCount() == 2) {

					// Double-click detected
					int index = list.locationToIndex(evt.getPoint());
					Chat.setText("");
					for (User user : Main.getUserList()) {
						if (user.getUsername().equals(DLM.get(index))) {
							remoteuser = user;
							channel = Main.openChannel(remoteuser);
							break;
						}
					}
					if (channel != null) {
						lblChat.setText("Chat with " + DLM.get(index) + " :");
						txtMessage.setEditable(true);
						btnSend.setEnabled(true);
						DatabaseConnectivity.getMessages(remoteuser);
					}
				}
			}
		});

		// "Send" button
		btnSend.setBounds(424, 419, 127, 75);
		contentPane.getRootPane().setDefaultButton(btnSend);
		contentPane.add(btnSend);
		
		// Label above the chat panel
		lblChat = new JLabel("Chat with :");
		lblChat.setBounds(12, 26, 540, 15);
		contentPane.add(lblChat);
		
		// Label printing our username
		lblUsername = new JLabel(Main.getLocalUser().getUsername());
		lblUsername.setFont(new Font("DejaVu Sans", Font.PLAIN, 17));
		lblUsername.setBounds(12, -11, 458, 43);
		contentPane.add(lblUsername);
	}
	
	/*
	 * Used to update the userlist displayed by pulling the list of available users
	 */
	public static void UpdateUserList() {
		// -- We fill the userlist : -- //
		DLM = new DefaultListModel<String>();
		for(User user : Main.getUserList())	{
			DLM.addElement(user.getUsername());
		}
		Userlist.setModel(DLM);
	}

	/*
	 * Defines the different fonts (styles, size, etc) used for each part of the text
	 */
	public void initFont()	{
		// Content of received message
		sender = new SimpleAttributeSet();
		StyleConstants.setBold(sender, true);
		StyleConstants.setFontSize(sender, 14);

		// Username of received message
		sender_username = new SimpleAttributeSet();
		StyleConstants.setBold(sender_username, true);
		StyleConstants.setFontSize(sender_username, 14);

		// Metadata (time) of received message
		sender_metadata = new SimpleAttributeSet();
		StyleConstants.setItalic(sender_metadata, true);
		StyleConstants.setFontSize(sender_metadata, 11);

		// Content of sent message
		receiver = new SimpleAttributeSet();
		StyleConstants.setFontSize(receiver, 14);
		
		// Username of sent message
		receiver_username = new SimpleAttributeSet();
		StyleConstants.setFontSize(receiver_username, 14);

		// Metadata (time) of sent message
		receiver_metadata = new SimpleAttributeSet();
		StyleConstants.setItalic(receiver_metadata, true);
		StyleConstants.setFontSize(receiver_metadata, 11);
	}
	
	/*
	 * Toggles the dark mode on and off
	 */
	public static void toggleDarkMode()	{
		Color textColor;
		
		// If dark mode is already enabled, we switch to light mode
		if (darkmode)	{
			darkmode = false;
			textColor = Color.black;
			
			Chat.setBackground(Color.decode("0xFFFFFF"));
			contentPane.setBackground(Color.decode("0xEEEEEE"));
			txtMessage.setBackground(Color.decode("0xEEEEEE"));
			btnSend.setBackground(Color.decode("0xEEEEEE"));
			btnDM.setBackground(Color.decode("0xEEEEEE"));
			Userlist.setBackground(Color.decode("0xFFFFFF"));

			btnDM.setText("Dark Mode");
		// If dark mode is disabled
		} else {
			darkmode = true;
			textColor = Color.white;
			
			// Panel backgrounds
			Chat.setBackground(Color.decode("0x36393F"));
			contentPane.setBackground(Color.decode("0x2B2C31"));
			txtMessage.setBackground(Color.decode("0x484C52"));
			btnSend.setBackground(Color.decode("0x202225"));
			btnDM.setBackground(Color.decode("0x202225"));
			Userlist.setBackground(Color.decode("0x2F3136"));
			
			btnDM.setText("Light Mode");
		}
		
		// Text labels
		lblUsers.setForeground(textColor);
		lblChat.setForeground(textColor);
		lblUsername.setForeground(textColor);
		Userlist.setForeground(textColor);
		btnSend.setForeground(textColor);
		btnDM.setForeground(textColor);
		txtMessage.setForeground(textColor);
		
		// Text inside the chat panel
		StyleConstants.setForeground(sender, textColor);
		StyleConstants.setForeground(sender_metadata, textColor);
		StyleConstants.setForeground(sender_username, textColor);
		StyleConstants.setForeground(receiver, textColor);
		StyleConstants.setForeground(receiver_metadata, textColor);
		StyleConstants.setForeground(receiver_username, textColor);
		
		// Messages are reloaded so the change in color is applied to them
		Chat.setText("");
		DatabaseConnectivity.getMessages(remoteuser);
	}
	
	/* 
	 * Static method used to print messages to the GUI
	 * The direction flag indicates if the message is outgoing (direction == 1) 
	 * or ingoing (direction == 0) to print it accordingly
	 */
	public static void RcvMsg(Message message, int direction) {
		// Empty messages are not printed (used for connection/deconnection, could be a missclick)
		if (!(message.getContent().isEmpty())) {
			// If the message refers to the current chat window
			if (message.getSender().getUsername().equals(remoteuser.getUsername()) || message.getSender().getUsername().equals(Main.getLocalUser().getUsername())) {
				try {
					// Ingoing message
					if (direction == 0) {
						Chat.getDocument().insertString(Chat.getDocument().getLength(), message.getSender().getUsername(), sender_username);
						Chat.getDocument().insertString(Chat.getDocument().getLength(), "    at " + message.getDate().toString().substring(11, 16) + " :", sender_metadata);
						Chat.getDocument().insertString(Chat.getDocument().getLength(), "\n    " + message.getContent() + "\n\n", sender);
					// Outgoing message
					} else {
						Chat.getDocument().insertString(Chat.getDocument().getLength(), Main.getLocalUser().getUsername(), receiver_username);
						Chat.getDocument().insertString(Chat.getDocument().getLength(), "    at " + message.getDate().toString().substring(11, 16) + " :", receiver_metadata);
						Chat.getDocument().insertString(Chat.getDocument().getLength(), "\n    " + message.getContent() + "\n\n", receiver);
					}
				} catch (Exception e) {
					System.out.println("[GUI] Error printing messages on the window");
					e.printStackTrace();
				}
			}
		}
		
		// Procedure to check if the user we are currently talking to has disconnected
		if(remoteuser.getUsername().equals(message.getSender().getUsername()))	{
			if(!channel.isRunning()) {
				try {
					Chat.getDocument().insertString(Chat.getDocument().getLength(), "\n" + message.getSender().getUsername() + "  "
									+ message.getDate().toString().substring(11, 16) + " has DISCONNECTED.",
							sender);
					btnSend.setEnabled(false);
					txtMessage.setEditable(false);
				} catch (BadLocationException e) {
					System.out.println("[GUI] Error printing disconnect message on the window");
					e.printStackTrace();				
				}
			}			
		}
	}
}
