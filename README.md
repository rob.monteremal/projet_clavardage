To launch the program, directly launch the .jar file.
SQLite is needed to use the program (the SQLite Java library is already included with the .jar file).

CAUTION: Only a single instance of the program should be opened on a PC as the multicast server runs on a dedicated port/socket.
To test the program, you need to launch it on 2 different PCs connected to the same local network.